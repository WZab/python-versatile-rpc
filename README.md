# Python versatile RPC

The project provides the implementation of simple RPC server using the MessagePack and ZeroMQ. Clients in different languages are available. The project is published as Public Domain or under Creative Commons Zero v1.0 Universal license.

The project is based on the sources posted in the Usent alt.surces group:
https://groups.google.com/g/alt.sources/c/QasPxJkKIUs
