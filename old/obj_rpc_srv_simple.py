#!/usr/bin/python
import msgpack as mp
import SocketServer
import traceback
import os

#Functions which process requests
def remote_mult(obj):
  return ('OK', obj[1]*obj[2])
  
def cat_file(obj):
  f=open(obj[1],"rb")
  res=f.read()
  return ('OK', res)

#Table of functions
func={
   'mult':remote_mult,
   'file':cat_file,
  }
  
class Obj_RPC_Handler(SocketServer.BaseRequestHandler):
    """
    The RequestHandler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
        # self.request is the TCP socket connected to the client
        # We send the greeting message to the client
        self.request.sendall("Object RPC server v. 1.0\n")
        # Create the unpacker and packer objects
        unp=mp.Unpacker()
        # Now we enter the loop waiting for objects and processing them
        while True:
	  rdta = self.request.recv(4096)
	  if not rdta:
	    break # Client shut the connection down
          #Process received object, sending the response back
	  unp.feed(rdta)
	  for obj in unp:
	    try:
	      res=func[obj[0]](obj)
	    except Exception:
	      res=("error", traceback.format_exc())
	    self.request.sendall(mp.packb(res))
	    
if __name__ == "__main__":
    HOST, PORT = "localhost", 9999

    # Create the server, binding to localhost on port 9999
    server = SocketServer.TCPServer((HOST, PORT), Obj_RPC_Handler)

    # Activate the server; this will keep running until you
    # interrupt the program with Ctrl-C
    server.serve_forever()
