#!/usr/bin/python
import socket
import sys
import msgpack as p

HOST, PORT = "localhost", 9999
data = " ".join(sys.argv[1:])
objects=[
  ["mult",4,5],
  ["mult",7,8],
  ["file","/etc/passwd"],
  ["file","/etc/akuku"],
  ]


class client:
  def __init__(self,host,port):
    self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    self.sock.connect((host,port))
    #Prepare the unpacker
    self.unp=p.Unpacker()
    #Receive the greeting string
    r=""
    while True:
      c=self.sock.recv(1);
      r+=c
      if c=="\n":
        print r
        break
  def do_cmd(self,obj):
    self.sock.sendall(p.packb(obj))
    while True:
      self.unp.feed(self.sock.recv(4096))
      try:
	res = self.unp.unpack()
        return res
      except StopIteration:
	pass
    
cli=client(HOST,PORT)
for i in objects:
   print cli.do_cmd(i)


