#!/usr/bin/python3
"""
   The code below implements a simple ZeroMQ and MessagePack RPC server.
   Written by Wojciech M. Zabolotny, wzab<at>ise.pw.edu.pl or wzab01<at>gmail.com
   13.04.2021

   The code is based on a post: "Simple object based RPC for Python",
   https://groups.google.com/g/alt.sources/c/QasPxJkKIUs/m/An1JnLooNCcJ
   https://www.funet.fi/pub/archive/alt.sources/2722.gz

   This code is published as PUBLIC DOMAIN  or under 
   Creative Commons Zero v1.0 Universal license (whichever better suits your needs).
"""
import time
import zmq

import msgpack as mp
import traceback
import os

#Functions which process requests
def remote_mult(a,b):
    return a*b

def remote_div(a,b):
    print(a,b,a/b)
    return a/b

def cat_file(fname):
    f=open(fname,"rb")
    return f.read()

#Table of functions
func={
  'mult':remote_mult,
  'div':remote_div,
  'file':cat_file,
}


def handle(msg):
    try:
        obj = mp.unpackb(msg)
        if len(obj) != 2:
            raise Exception("Wrong number of RPC objects, should be 2: name and arguments")
        if isinstance(obj[1],tuple) or isinstance(obj[1],list):
            res=func[obj[0]](*obj[1])
        elif isinstance(obj[1],dict):
            res=func[obj[0]](**obj[1])
        else:
            raise Exception("Wrong type of arguments in RPC, should be list, tuple or dictionary")
        res = ("OK", res)
    except Exception:
        res=("error", traceback.format_exc())
    return mp.packb(res)

if __name__ == "__main__":
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    # Create the server, binding to localhost on port 9999
    socket.bind("tcp://*:9999")
    while True:
        msg = socket.recv()
        res = handle(msg)
        socket.send(res)
