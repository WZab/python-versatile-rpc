% Demonstrator of the communication with simple Python RPC server from Octave
% Written by Wojciech M. Zabołotny (wzab01<at>gmail.com or wzab<at>ise.pw.edu.pl)
% Copyright: This program is released into the public domain.
pkg load jsonlab
pkg load zeromq

srv = zmq_socket (ZMQ_REQ);
zmq_connect (srv, "tcp://localhost:9999");


function res = rpc(req,fname,fargs,maxlen=10000)
  x={fname, fargs};
  a=savemsgpack(x);
  zmq_send(req,a);
  w=zmq_recv(req,maxlen);
  y=loadmsgpack(char(w));
  if strcmp(char(y{1}),"OK")
     res = y{2};
  end
  if strcmp(char(y{1}),"error")
     error(char(y{2}));
  end
endfunction
  
res = rpc(srv,"mult",struct("a",13,"b",20));
res
res = rpc(srv,"mult",{17,3});
res
res = rpc(srv,"file",{"/etc/passwd"});
char(res')

