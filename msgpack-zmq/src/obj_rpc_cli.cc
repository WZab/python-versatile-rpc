// Demonstrator of the communication with simple Python RPC server from C++
// Written by Wojciech M. Zabołotny (wzab01<at>gmail.com or wzab<at>ise.pw.edu.pl)
// Copyright: This program is released into the public domain or under
// Creative Commons Zero v1.0 Universal license (whichever better suits your needs).
#include <string>
#include <zmq.hpp>
#include <iostream>
#include <msgpack.hpp>

msgpack::object_handle rpc(zmq::socket_t &sock, auto req)
{
   std::size_t offset = 0;
   std::stringstream rstr;
   msgpack::pack(rstr,req);
   zmq::message_t msg(rstr.str());
   sock.send(msg,zmq::send_flags::none);
   auto res = sock.recv(msg, zmq::recv_flags::none);
   auto oh = msgpack::unpack((const char *)msg.data(),msg.size(),offset);
   return oh;
}

int main(void) {
   zmq::context_t ctx;
   zmq::socket_t sock(ctx, zmq::socket_type::req);
   sock.connect("tcp://localhost:9999");
   msgpack::object_handle res;
   res = rpc(sock,std::tuple<std::string, std::array<int,2>>({"mult", {7, 8}}));
   std::cout << res.get() << std::endl;
   res = rpc(sock,std::tuple<std::string, std::map<std::string,int>>({"div", {{"a",8},{"b",7}}}));
   std::cout << res.get() << std::endl;
   res = rpc(sock,std::tuple<std::string, std::map<std::string,int>>({"div", {{"b",8},{"a",7}}}));
   std::cout << res.get() << std::endl;
   res = rpc(sock,std::tuple<std::string, std::tuple<std::string>>({ "file", {"/etc/passwd"}}));
   std::cout << res.get() << std::endl;
}
