#!/usr/bin/python3
"""
   The code below implements a simple ZeroMQ and MessagePack RPC client.
   Written by Wojciech M. Zabolotny, wzab<at>ise.pw.edu.pl or wzab01<at>gmail.com
   13.04.2021

   The code is based on a post: "Simple object based RPC for Python",
   https://groups.google.com/g/alt.sources/c/QasPxJkKIUs/m/An1JnLooNCcJ
   https://www.funet.fi/pub/archive/alt.sources/2722.gz

   This code is published as PUBLIC DOMAIN  or under 
   Creative Commons Zero v1.0 Universal license (whichever better suits your needs).
"""
import socket
import sys
import msgpack as p
import zmq

HOST, PORT = "localhost", 9999
data = " ".join(sys.argv[1:])
objects=[
    ["mult",(4,5)],
    ["mult",{"a":7,"b":8}],
    ["div",{"a":9,"b":4}],
    ["file",("/etc/passwd",)],
    ["file",("/etc/non_existing_file",)],
]


context = zmq.Context()

socket = context.socket(zmq.REQ)
socket.connect("tcp://localhost:9999")    

for obj in objects:
    socket.send(p.packb(obj))
    #  Get the reply.
    msg = socket.recv()
    resp = p.unpackb(msg)
    print("Received reply", resp)
