"""
   The code below implements a simple JSON-based RPC server for MicroPython.
   Written by Wojciech M. Zabolotny, wzab<at>ise.pw.edu.pl or wzab01<at>gmail.com
   18.07.2021

   The code is based on a post: "Simple object based RPC for Python",
   https://groups.google.com/g/alt.sources/c/QasPxJkKIUs/m/An1JnLooNCcJ
   https://www.funet.fi/pub/archive/alt.sources/2722.gz

   This code is published as PUBLIC DOMAIN  or under 
   Creative Commons Zero v1.0 Universal license (whichever better suits your needs).
"""
import time
import ujson
import network
import usocket
import _thread

CMD_MAXLEN = 100
#Functions which process requests
def remote_mult(a,b):
    return a*b

def remote_div(a,b):
    print(a,b,a/b)
    return a/b

def cat_file(fname):
    f=open(fname,"rb")
    return f.read()

#Table of functions
func={
  'mult':remote_mult,
  'div':remote_div,
  'file':cat_file,
}


def handle(msg):
    try:
        obj = ujson.loads(msg)
        if len(obj) != 2:
            raise Exception("Wrong number of RPC objects, should be 2: name and arguments")
        if isinstance(obj[1],tuple) or isinstance(obj[1],list):
            res=func[obj[0]](*obj[1])
        elif isinstance(obj[1],dict):
            res=func[obj[0]](**obj[1])
        else:
            raise Exception("Wrong type of arguments in RPC, should be list, tuple or dictionary")
        res = ("OK", res)
    except Exception as e:
        res=("error", str(e))
    return ujson.dumps(res)

class rpcsrv():
    def __init__(self):
        self.lan = network.LAN()
        self.lan.active(True)
        self.thread = None
        self.active = False
    def run(self,port):
        if self.thread is not None:
            raise(Exception("Can't start server twice!"))
        self.active = True
        self.thread = _thread.start_new_thread(self.domsg,(port,))
    def stop(self):
        self.active = False
    def domsg(self,port):
        addr = usocket.getaddrinfo('0.0.0.0',port)[0][-1]
        s = usocket.socket(usocket.AF_INET, usocket.SOCK_STREAM)
        s.setsockopt(usocket.SOL_SOCKET, usocket.SO_REUSEADDR, 1)
        s.bind(addr)
        s.listen(1)
        while self.active:
            c, addr = s.accept()
            c.sendall(b'RPC srv 1.0\n')
            while True:
                cmd = c.readline(CMD_MAXLEN)
                if (not cmd) or (cmd == b'\n') or (cmd == b'\r\n'):
                    break
                if cmd[-1] != b'\n'[0]:
                    c.sendall(ujson.dumps('error','CMD too long').encode()+'\n')
                    break
                resp = handle(cmd)
                c.sendall(resp.encode()+'\n')
            c.close()             
        s.close()
        

