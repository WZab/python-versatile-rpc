#!/usr/bin/python3
"""
   The code below implements a simple JSON_based RPC client.
   Written by Wojciech M. Zabolotny, wzab<at>ise.pw.edu.pl or wzab01<at>gmail.com
   18.07.2021

   The code is based on a post: "Simple object based RPC for Python",
   https://groups.google.com/g/alt.sources/c/QasPxJkKIUs/m/An1JnLooNCcJ
   https://www.funet.fi/pub/archive/alt.sources/2722.gz

   This code is published as PUBLIC DOMAIN  or under 
   Creative Commons Zero v1.0 Universal license (whichever better suits your needs).
"""
import sys
import json
import socket

HOST, PORT = "192.168.33.20", 9999
data = " ".join(sys.argv[1:])
objects=[
    ["mult",(4,5)],
    ["mult",{"a":7,"b":8}],
    ["div",{"a":9,"b":4}],
    ["file",("/flash/boot.py",)],
    ["file",("/etc/non_existing_file",)],
]

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect((HOST,PORT))
sockfile = sock.makefile()
greet = sockfile.readline()
print(greet)
for obj in objects:
    sock.sendall(json.dumps(obj).encode()+b'\n')
    #  Get the reply.
    msg = sockfile.readline()
    resp = json.loads(msg)
    print("Received reply", resp)

