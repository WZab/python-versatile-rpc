% Demonstrator of the communication with simple Python RPC server from Octave
% Written by Wojciech M. Zabołotny (wzab01<at>gmail.com or wzab<at>ise.pw.edu.pl)
% Copyright: This program is released into the public domain.
pkg load jsonlab
pkg load sockets

srv = socket (AF_INET, SOCK_STREAM);
connect (srv, struct("addr", "192.168.33.20", "port", 9999));

function msg = readline(sock)
   msg = '';
   while true
      [chr, count] = recv(sock,1);
      if count < 0
         error("I can't receive the line")         
      endif
      msg = [msg char(chr)];
      if chr == 10
         return
      endif
   endwhile
endfunction

printf("%s",readline(srv))

function sendall(sock,msg)
   st = 0;
   tosend = length(msg);
   while (st < tosend)
      now_sent = send(sock,msg((st+1):tosend));
      if now_sent < 0
         error("I can't send message")
      endif
      st = st + now_sent;      
   endwhile
endfunction

function msg = recvall(sock,nbytes)
   rcvd = 0;
   msg='';
   while (rcvd < nbytes)
      [dta,now_rcvd] = recv(sock,nbytes-rcvd);
      if now_rcvd < 0
         error("I can't receive message")
      endif
      rcvd = rcvd + now_rcvd;
      msg = [msg, char(dta)];
   endwhile
endfunction

function res = rpc(sock,fname,fargs,maxlen=10000)
  x={fname, fargs};
  a=savemsgpack(x);
  y=uint32(length(a));
  y=swapbytes(y);
  y=char(typecast(y,"uint8"));
  sendall(sock,y);
  sendall(sock,a);
  l=recvall(sock,4);
  l=typecast(l,"uint32");
  l=swapbytes(l);
  w=recvall(sock,l);
  y=loadmsgpack(char(w));
  if strcmp(char(y{1}),"OK")
     res = y{2};
  end
  if strcmp(char(y{1}),"error")
     error(char(y{2}));
  end
endfunction
  
res = rpc(srv,"mult",struct("a",13,"b",20));
res
res = rpc(srv,"mult",{17,3});
res
res = rpc(srv,"div",{1,7});
res
res = rpc(srv,"file",{"/flash/boot.py"});
char(res')
disconnect(srv)

