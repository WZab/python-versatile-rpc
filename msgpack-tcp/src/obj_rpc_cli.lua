-- Demonstrator of the communication with simple Python RPC server from Lua
-- It works with the MicroPython server, using MessagePack and the simplistic TCP
-- encapsulation (big-endian, 4-byte length of the msgpack-packed request is sent
-- before the request itself).
-- Written by Wojciech M. Zabołotny (wzab01<at>gmail.com or wzab<at>ise.pw.edu.pl)
-- Copyright: This program is released into the public domain or under
-- Creative Commons Zero v1.0 Universal license (whichever better suits your needs).
local struct = require "struct"
--require "utils"
local mp = require "mpack"
--print_version(zmq)
local pr = require "pl.pretty"
socket = require "socket"
srv = socket.connect("192.168.33.20", 9999)
greet = srv:receive('*l')
print(greet)

function rpc(srv,cmd)
    local req=mp.pack(cmd)
    local reqlen = struct.pack('>I',string.len(req))
    srv:send(reqlen)
    srv:send(req)
    local reclen = struct.unpack('>I',srv:receive(4))
    rcv = srv:receive(reclen)
    local res=mp.unpack(rcv)
    return res  
end

test = {"file",{"/flash/boot.py"}}
local res = rpc(srv,test)
pr.dump(res)
test = {"mult",{7,8}}
res = rpc(srv,test)
pr.dump(res)
test = {"div",{b=4.0,a=9.0}}
res = rpc(srv,test)
pr.dump(res)
-- The above works, but 9/4 is printed as 2.
print(res[2])
test = {"div",{a=4.0,b=9.0}}
res = rpc(srv,test)
pr.dump(res)
-- The above works, but 4/9 is printed as 0.
print(res[2])

