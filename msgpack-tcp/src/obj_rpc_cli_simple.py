#!/usr/bin/python3
"""
   The code below implements a simple ZeroMQ and MessagePack RPC client.
   Written by Wojciech M. Zabolotny, wzab<at>ise.pw.edu.pl or wzab01<at>gmail.com
   13.04.2021

   The code is based on a post: "Simple object based RPC for Python",
   https://groups.google.com/g/alt.sources/c/QasPxJkKIUs/m/An1JnLooNCcJ
   https://www.funet.fi/pub/archive/alt.sources/2722.gz

   This code is published as PUBLIC DOMAIN  or under 
   Creative Commons Zero v1.0 Universal license (whichever better suits your needs).
"""
import socket
import sys
import msgpack as p
import struct
CMD_MAXLEN = 1000

HOST, PORT = "192.168.33.20", 9999
data = " ".join(sys.argv[1:])
objects=[
    ["mult",(4,5)],
    ["mult",{"a":7,"b":8}],
    ["div",{"a":9,"b":4}],
    ["file",("/etc/passwd",)],
    ["file",("/etc/non_existing_file",)],
]

class rpccli():
    def __init__(self,host,port):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((host,port))
        self.sockfile = self.sock.makefile()
        greet = self.sockfile.readline()
        print(greet)

    def read(self,nbytes):
        res=b''
        try:
            while True:
                blen = len(res)
                if blen >= nbytes:
                    return res
                new = self.sock.recv(nbytes-blen)
                if not new:
                    return None
                res += new
        except Exception as e:
            print(e)
            return None

    def recvmsg(self):
        # Get the length of the request
        length = self.read(4)
        print(length)
        if length is None:
            return None
        length = struct.unpack('>L',length)[0]
        print(length)
        if length > CMD_MAXLEN:
            self.sendmsg(p.packb(('error','CMD too long')))
            return None
        msg = self.read(length)
        return msg
                
    def sendmsg(self,msg):
        length = struct.pack('>L',len(msg))
        self.sock.sendall(length)
        self.sock.sendall(msg)    

rpc = rpccli(HOST,PORT)
for obj in objects:
    rpc.sendmsg(p.packb(obj))
    #  Get the reply.
    msg = rpc.recvmsg()
    resp = p.unpackb(msg)
    print("Received reply", resp)
    
